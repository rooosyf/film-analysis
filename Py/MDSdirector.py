import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn import manifold
import math
import csv

def string_sim(s1, s2):
    if(s1 == s2):
        return 0
    else:
        return 1
def dissimilarity(m1, m2):
    
    genre_sim = string_sim(m1[1], m2[1]) * 10
    lang_sim = string_sim(m1[3], m2[3])*10
    dir_sim = string_sim(m1[4], m2[4])*100
    writ_sim = string_sim(m1[5], m2[5])*10
    prod_comp_sim = string_sim(m1[6], m2[6])*10
    act_sim = string_sim(m1[7], m2[7])*10
    dis = abs(m1[0]-m2[0]) + genre_sim + abs(m1[2]-m2[2]) + lang_sim + dir_sim + writ_sim + prod_comp_sim + act_sim + abs(float(m1[8])-float(m2[8]))
    return dis

def MDS():
    data = pd.io.parsers.read_csv(
        'out.csv', 
        header=0,
        usecols=[1,2,4,5,7,8,9,10,11,13]
        )
    d=data.values[:,1:]
    movies=data.values.T[0]
    genere=data.values.T[2]

    dissM1=np.zeros((len(data),len(data))) #creates a zeros dissM

    for i in range(len(d)):
        for j in range (len(d)):
            dissM1[i][j]= dissimilarity(d[i], d[j])
        
    mds = manifold.MDS(n_components=2, max_iter=300, eps=1e-9,
                    dissimilarity="precomputed")
    pos1 = mds.fit(dissM1).embedding_

    s = 50

    plt.scatter(pos1[:, 0], pos1[:, 1], color='red',s=s, lw=0, label='d[i]-d[j]')

    row_list = []

    for label, x, y, genre in zip(movies, pos1[:, 0], pos1[:, 1],genere):
        row_list.append([label, x, y,genre])
        plt.annotate(
            label,
            xy = (x, y), xytext = (-20, 20),
            textcoords = 'offset points', ha = 'right', va = 'bottom',
            bbox = dict(boxstyle = 'round,pad=0.3', fc = 'yellow', alpha = 0.5))       

    with open('MDSdirector.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(['Label', 'X', 'Y','genre'])
        writer.writerows(row_list)

    plt.legend()   
    plt.show()
    
MDS()
