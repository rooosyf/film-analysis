from flask import Flask
from flask import request
from prova_mds import MDS
import json
import urllib.request
import textwrap

app = Flask(__name__)

@app.route('/post',methods=['GET'])
def post_mds():
    return MDS()

if __name__ == '__main__':
    app.run(host='0.0.0.0')