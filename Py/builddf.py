import pandas as pd
import numpy as np
df_full = pd.read_csv('IMDb.csv')
df_full = df_full.dropna(subset=['usa_gross_income','actors','director'])
df = df_full.loc[df_full['country'] == 'USA']
df = df.drop(columns=['title','budget','votes','worlwide_gross_income','metascore','reviews_from_users','reviews_from_critics'])
df['language'] = df['language'].replace(np.nan, 'English')
df['production_company'] = df['production_company'].replace(np.nan, 'National Film Corporation of America')
df['date_published']= pd.to_datetime(df['date_published'])
df['year'] = df['year'].astype(int)
is_2002 =  df['year']>=2000
df = df[is_2002]
df['date_published'] = pd.DatetimeIndex(df['date_published']).month
df.rename(columns={'date_published': 'month_published'}, inplace=True)
#df.to_csv('out.csv', index=False)
df.info()

print(df.head())

