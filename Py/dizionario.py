import pandas as pd
df = pd.read_csv('out.csv')
genres = df['genre']
lista =[]
for elem in genres:
    new = elem.split(',')[0]
    if new =='Romance':
        new = 'Drama'
    elif new =='Thriller' or new =='Mystery':
        new = 'Horror'
    elif new == 'Adventure' or new == 'Sport' or new=='Western' or new =='War' or new =='Crime':
        new = 'Action'
    elif new == 'Sci-Fi':
        new = 'Fantasy'
    elif new == 'History' or new =='Biography' or new == 'Animation':
        new = 'Family'
    lista.append(new)
    

ser = pd.Series(lista)
#print(ser)
df['genre'] = ser
df.to_csv('out.csv', index=False)
