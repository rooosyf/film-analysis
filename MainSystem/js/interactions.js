function arrayRemove(arr, value) {
  return arr.filter(function (ele) {
    return ele != value;
  });
}

var interactionData = []; // list of dataset element that have to be hightlighted
var seltitles = [];
var selActors = [];

var brushMDS = 0;
var brushParallel = 0

function resetAll() {
  // reset Interaction Data
  interactionData = [];

  LG_IntData = [];
  HM_IntData = [];
  PL_IntData = [];

  // reset brush flags
  brushMDS = 0;
  brushParallel = 0;

  // call important function
  clearParallel();
  resetMDS();
  resetCast();

  

  mdsV.select(".brushbutton").dispatch("click");
  updateAll();

  /*d3.select(".film_title").html(
    "Use the brush on the MDS graph to print films titles"
  );*/
  printFilms([])
}

function updateAll() {
  updateLineGraph();
  updateHeatmap();
}

// ---- LineGraph Interaction ---- //
var LG_IntData = [];

function triggerLineGraph(objList) {
  var id = "#" + objList[0].genre + "-" + objList[0].year;

  // was already selected ? ---> remove element from InteractionData
  if (LG_IntData.includes(id)) {
    LG_IntData = arrayRemove(LG_IntData, id);
    var data = interactionData.filter(function (d) {
      return d.genre != objList[0].genre || d.year != objList[0].year;
    });
    interactionData = data;
  }
  // was not selected ? ---> add element to InteractionData
  else {
    LG_IntData.push(id);
    objList.forEach(function (obj) {
      if (!interactionData.includes(obj)) {
        interactionData.push(obj);
      }
    });
  }
  updateAll();
}

function updateLineGraph() {
  //reset all to normal
  d3.selectAll(".LG_circle")
    .attr("stroke", "black")
    .attr("stroke-width", 1)
    .attr("r", 5);
  d3.selectAll(".linegraphLine").style("opacity", 0.3);

  // Add ids to the personal list
  interactionData.forEach(function (obj) {
    var obj_id = "#" + obj.genre + "-" + obj.year;
    if (!LG_IntData.includes(obj_id)) {
      LG_IntData.push(obj_id);
    }

    d3.selectAll(".LG_circle");
    d3.select(obj_id)
      .attr("stroke", "#ffa600")
      .attr("stroke-width", 5)
      .attr("r", 12);

    d3.select("#linegraph");
    d3.select("#" + obj.genre + "_line").style("opacity", 1);
  });

  if (interactionData.length == 0) {
    d3.selectAll(".linegraphLine").style("opacity", 0.8);
  }
}
// ------------------------------- //

// ----- Heatmap Interaction ----- //
var HM_IntData = [];

function triggerHeatmap(obj) {
  dataReader.then(function (data) {
    var year = obj.year;
    var month = obj.month_published;

    var id = month + "-" + year;
    var objList = data.filter(function (d) {
      return (d.year == year) & (d.month_published == month);
    });

    // was already selected ? ---> remove element from InteractionData
    if (HM_IntData.includes(id)) {
      HM_IntData = arrayRemove(HM_IntData, id);
      var data = interactionData.filter(function (d) {
        return (
          d.month_published != objList[0].month_published ||
          d.year != objList[0].year
        );
      });
      interactionData = data;
    }
    // was not selected ? ---> add element to InteractionData
    else {
      HM_IntData.push(id);
      objList.forEach(function (obj) {
        if (!interactionData.includes(obj)) {
          interactionData.push(obj);
        }
      });
    }

    updateAll();
  });
}

function updateHeatmap() {
  var heat = d3.select(".heatmapView");
  // reset all square to normal
  heat
    .selectAll(".rect")
    .style("stroke", "none")
    .style("opacity", function () {
      if (d3.select(this).style("opacity") == 0) {
        return 0;
      } else {
        return 0.02; // original 0.8
      }
  });

  if (interactionData.length == 0){
    heat
    .selectAll(".rect")
    .style("opacity", function () {
      if (d3.select(this).style("opacity") == 0) {
        return 0;
      } else {
        return 0.8;
      }
  });
  }

  // Add ids to the personal list
  var ids = [];

  interactionData.forEach(function (obj) {
    var obj_id = obj.month_published + "-" + obj.year;
    ids.push(obj_id);
    if (!HM_IntData.includes(obj_id)) {
      HM_IntData.push(obj_id);
    }
  });

  // highlight everything in the personal list
  heat
    .selectAll(".rect")
    .filter(function (d) {
      return ids.includes(d.month_published + "-" + d.year);
    })
    .style("stroke", function () {
      if (d3.select(this).style("opacity") == 0) {
        return "none";
      } else {
        return "white";
      }
    })
    .style("stroke-width", function () {
      if (d3.select(this).style("opacity") == 0) {
        return "0px";
      } else {
        return "5px";
      }
    })
    .style("opacity", function () {
      if (d3.select(this).style("opacity") == 0) {
        return 0;
      } else {
        return 1;
      }
    });
}

// ------- MDS Interaction ------- //
function triggerMDS(myObj) {
  dataReader.then(function (data) {
    var obj = data.filter(function (d) {
      return d.original_title == myObj.Label;
    })[0];

    // was already selected ? ---> remove element from InteractionData
    if (interactionData.includes(obj)) {
      interactionData = arrayRemove(interactionData, obj);
    }
    // was not selected ? ---> add element to InteractionData
    else {
      interactionData.push(obj);
    }

    updateAll();
  });
}

function updateMDS() {
  var mds = d3.select(".mdsView");

  mds.select(".brushbutton").dispatch("click");

  // reset all dots to normal
  mds
    .selectAll("circle")
    .style("fill", function (d) {
      return colorDict[d.genre];
    })
    .style("opacity", 0.3); // original 0.1
  //.style("stroke", "black");
  // hightlight the interactionData object
  seltitles = [];

  interactionData.forEach(function (obj) {
    seltitles.push(obj.original_title);
  });

  mds
    .selectAll("circle")
    .filter(function (d) {
      return seltitles.includes(d.Label);
    })
    .style("fill", function (d) {
      return colorDict[d.genre];
    })
    .style("opacity", 1); //original 0.6
  //.style("stroke", "white");

  mds
    .selectAll("circle")
    .filter(function (d) {
      return seltitles.indexOf(d.Label) < 0;
    })
    .style("fill", "gray");

  d3.select(".film_title").html(seltitles.join("<br/>"));

  if (interactionData.length === 0) {
    mds
      .selectAll("circle")
      .style("fill", function (d) {
        return colorDict[d.genre];
      })
      .style("opacity", 0.3)
      .style("stroke", "black");

    d3.select(".film_title").html(
      "Use the brush on the MDS graph to print films titles"
    );
  }

  //MDSBasedOnGenre();
}
// ------------------------------ //

// ---- Parallel Interaction ---- //
var PL_IntData = [];

genreList = [
  "Action",
  "Comedy",
  "Drama",
  "Family",
  "Fantasy",
  "Horror",
  "Musical",
];
function updateGenreList(newGenreList) {
  genreList = newGenreList;
  MDSBasedOnGenre();
}

function MDSBasedOnGenre() {
  d3.select(".mdsView")
    .selectAll("circle")
    .filter(function (d) {
      return !genreList.includes(d.genre);
    })
    .style("fill", "gray")
    .style("opacity", 0.3); //original 0.3
}


function triggerMDSfromParallel(actors) {
  // --- handle brushes ---
  // remove mds brush
  if (brushMDS == 1){
    mdsV.select(".brushbutton").dispatch("click");
    brushMDS = 0;
  }
  
  // take the flag
  if (brushParallel == 0) { brushParallel = 1;}

  dataReader.then(function (data) {
    var actor_films = [];

    data.forEach(function (d) {
      var film_actors = d.actors.split(","); //.forEach(x => x.toLowerCase())

      if (actors.some((x) => film_actors.includes(x))) {
        actor_films.push(d.original_title);
      }
    });
    if (actor_films.length > 0) {
      mdsV
        .selectAll("circle")
        .style("fill", function (d) {
          return actor_films.includes(d.Label) ? colorDict[d.genre] : "gray";
        })
        .style("opacity", function (d) {
          return actor_films.includes(d.Label) ? 1 : 0.2;
        });
        //d3.select(".film_title").html(actor_films.join("<br>"))
        printFilms(actor_films)
    } else {
      mdsV
        .selectAll("circle")
        .style("fill", (d) => colorDict[d.genre])
        .style("opacity", 0.3);
      /*d3.select(".film_title").html(
          "Use the brush on the MDS  graph to print films titles"
        );*/

      printFilms([])
    }
  });
}

function handler_of_brushes(){
  // --- handle brushes ---
  // remove parallell brush
  if (brushParallel == 1){
    parcoords.brushReset();
    brushParallel = 0;
  }
  // take the flag
  if (brushMDS == 0) { brushMDS = 1;}
}


var attori_high;
var final_list;

function trigger_parallel_from_MDS(movies) {
  dataReader.then(function (data) {
    attori_high = data.filter((element) => {
      if (movies.includes(element.original_title)) {
        return element;
      }
    });

    attori_high = attori_high.map((d) => d.actors.split(",")[0]);
    console.log(attori_high);
  });

  parallelReader.then(function (data) {
    final_list = data.filter((elem) => {
      if (attori_high.includes(elem.name)) return elem;
    });
    if(movies.length > 0){
      parcoords.highlight(final_list);
      var attori = [...new Set(attori_high)]
      printActors(attori);
    }
    else{
      clearParallel()
    }
    //d3.select(".actor").html(attori_high.join("<br>"));
  });
}


function parallellFilter(movie) {

  dataReader.then(function (data) {
    var movieObj = data.filter(function(d) { return d.original_title == movie; })[0];

    attori_high = movieObj.actors.split(", ");

    parallelReader.then(function (data) {
      final_list = data.filter((elem) => {
        if (attori_high.includes(elem.name)) return elem;
      });
      if([movieObj].length > 0){
        parcoords.highlight(final_list);
        var attori = [...new Set(attori_high)]
        printActors(attori);
      }
      else{
        clearParallel()
      }
    });
  });
}