var attori; // The global data structure of the parallel coordinates graph, filled in the Promise below.
var names;

function drawParallels() {
  // Parse the Data
  parallelReader.then(function (data) {
    attori = data;
    parcoords
      .data(data)
      .render()
      .smoothness(0.1)
      .composite("source-over") // Find the best (the less ugly) composite method
      .hideAxis(["name"])
      .mode("queue")
      .reorderable()
      .brushMode("1D-axes")
      .on("brush", act_selected);
  });
  // Extract the list of dimensions we want to keep in the plot.
}

// A basic listener that prints the selected element in the brush.
// It works, but it's very ugly.
function act_selected() {
  let selected = parcoords.brushed();
  names = [];
  for (actor of selected) {
    names.push(actor.name);
  }
  // An ugly way to fix the reset button
  if (names.length === 2279) {
    names = [];
  }
  
 
  printActors(names);
  triggerMDSfromParallel(names);
}

// Search on paralllel, doesn't work on other graphs.
function searchActor() {
  console.log("searching");
  var search = document.getElementById("Actsearch").value.toLowerCase();
 

  for (actor of attori) {
    if (actor.name.toLowerCase() == search) {
      printActors([actor.name])
      //d3.select(".actor").html(actor.name);
    }
  }
  dataReader.then(function (data) {
    var actor_films = [];
    data.forEach(function (d) {
      var actors = d.actors.toLowerCase().split(","); //.forEach(x => x.toLowerCase())
      if (actors.includes(search)) {
        actor_films.push(d.original_title);
      }
    });
    
    //d3.select(".film_title").html(actor_films.join("<br/>"));
    printFilms(actor_films)
  });
}





// ----- ACTOR PRINT SECTION -----
var castList = [];
var castLimit = 5;

function arrayRemove(arr, value) {
  return arr.filter(function (ele) {
    return ele != value;
  });
}

function printActors(names) {
  // remove empty object
  names = arrayRemove(names, "");

  // create ultimate list 
  var ultimateList = [...names]; // clone "names" array
  castList.forEach(function(act){ if (!ultimateList.includes(act)){ ultimateList.push(act); } });

  // get div
  var actorDiv = d3.select(".actor");

  // remove old keys
  actorDiv.selectAll(".actor_key").remove();

  // --- no actor message ---
  if (ultimateList.length == 0) {
    var key_items = d3
      .select(".actor")
      .append("div")
      .attr("class", "actor_key")
      .text("Use the brush on the parallel coordinates to print actors names")
      .style("text-align", "center")
    ;
  }
  // ----- actors -----
  else {
    // sort the list in alphabetical order
    ultimateList.sort();

    var key_items = d3
      .select(".actor")
      .selectAll("div")
      .data(ultimateList)
      .enter()
      .append("div")
      .attr("class", "actor_key")
      .style("margin-top", "5px")
    ;

    // ON CLICK
    key_items.on("click", function(d){
      // Remove from the cast 
      if( castList.includes(d) ){
        castList = arrayRemove(castList, d);
        d3.select(this).select(".actor_square").style("background", "#33a02c");
        d3.select(this).select(".actor_square").text("+");
        if (!names.includes(d)){ d3.select(this).remove(); }
        // check if the cast was full
        if ( castList.length == castLimit-1) { handleSquare(0); }
      }
      // add to the cast
      else {
        if ( castList.length < castLimit ) { 
          castList.push(d);
          castList.sort();
          d3.select(this).select(".actor_square").style("background", "#fdbf6f");
          d3.select(this).select(".actor_square").text("-");
          // check if cast is full
          if ( castList.length == castLimit) { handleSquare(1); }
        }
      }
      printOnAnalytics();
    });
    // ON CLICK

    
    // ON MOUSEOVER
    key_items.on("mouseover", function (d) {
      // cursor to pointer
      d3.select(this).style("cursor", "pointer");
      // text become yellow
      d3.select(this)
        .select(".actor_label")
        .style("font-weight", "bolder")
        .style("color", "yellow");
      // the square become light green
      d3.select(this).select(".actor_square").style("background", function(d){
        if( castList.includes(d) ){ return "#fdbf6f"; }
        else { 
          if( castList.length != castLimit){ return "#33a02c"; } 
          else { return "grey"; }
        }
      });
      // glow the line on parallell
      // 'd' is the name of the actor
    });
    // ON MOUSEOVER
    

    // ON MOUSEOUT
    key_items.on("mouseout", function (d) {
      // cursor to default
      d3.select(this).style("cursor", "default");
      d3.select(this)
        .select(".actor_label")
        .style("font-weight", "normal")
        .style("color", "white");

        d3.select(this).select(".actor_square").style("background", function(d){
          if( castList.includes(d) ){ return "#ffa600"; }
          else { 
            if( castList.length != castLimit){ return "green"; } 
            else { return "grey"; }
          }
        });
    });
    // ON MOUSEOUT

    key_items
      .append("div")
        .style("text-align", "center")
        .attr("class", "actor_square")
        .style("float", "right")
    ;

    key_items
      .append("div")
      .attr("class", "actor_label")
      .text(function (d) {
        return d;
      })
      .style("color", "white")
    ;

    if (castList.length == castLimit){ handleSquare(1);}
    else { handleSquare(0);}
  }
  printOnAnalytics();
}

function handleSquare(flag){

  d3.select(".actor")
    .selectAll(".actor_key")
      .selectAll(".actor_square")
        .style("background", function(d){
          if (!castList.includes(d)){
            // avaiable --> green
            if ( flag == 0 ) { return "green"; }
            // unavaiable --> grey
            else if ( flag == 1) { return "grey"; }
          }
          else { return "#ffa600"; }
        })
        .text(function(d){
          if (!castList.includes(d)){
            // avaiable --> +
            if ( flag == 0 ) { return "+"; }
            // unavaiable --> x
            else if ( flag == 1) { return "x"; }
          }
          else { return "-"; }
        })
  ;
}


function printOnAnalytics(){
  var castSection = d3.select(".analytics").select(".actors_analytics");

  // remove old cast
  castSection.selectAll(".cast_label").remove();

  // print new cast
  castList.forEach(function(act){
    castSection
      .append("div")
        .attr("class","cast_label")
        .html("<li>"+act+"</li>")
        //.text("- "+ act)
        //.style("padding", "8px")
    ;
  });
  directors(d3.select("#selectGenres").property("value"), month);

  var missingAct = castLimit - castList.length;

  for (var i=0; i < missingAct; i++){
    castSection
    .append("div")
      .attr("class","cast_label")
      .html("<li/>")
      
    ;
  }
}


function resetCast(){
  castList = [];
  printActors([]);
}