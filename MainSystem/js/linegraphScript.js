/* --- Global Variables --- */
var LG_xScale;
var LG_yScale;

var yValue;

var colorDict = {
  Action: "#fdbf6f", // yellow
  Drama: "#1f78b4", // blue
  Horror: "#b2df8a", // light green
  Family: "#33a02c", // green
  Comedy: "#fb9a99", // pink
  Fantasy: "#e31a1c", // red
  Musical: "#a6cee3", // light blue
};
/* ------------------------ */

function resetL() {
  d3.selectAll(".LG_circle")
    .attr("r", 5)
    .style("stroke", "black")
    .style("stroke-width", 1);
}

function drawLinegraph(myWidth, myHeight, yValue_temp) {
  d3.select(".lineSVG").remove(); // delete old svg

  yValue = yValue_temp;

  var margin = { left: 48, top: 32, right: 30 };

  var graphDim = {
    width: parseInt(myWidth) - 2 * margin.left - margin.right,
    heigth: parseInt(myHeight) - 3 * margin.top,
  };

  // ----- Draw Graph ----- //
  // svg
  var svg = d3
    .select(".linegraphView")
    .append("svg")
    .attr("class", "lineSVG")
    .attr("width", myWidth)
    .attr("height", myHeight)
    .attr("transform", "translate(0," + 25 + ")");
  // graph
  var graph = svg
    .append("g")
    .attr("id", "linegraph")
    .attr(
      "transform",
      "translate(" + 1.5 * margin.left + "," + margin.top + ")"
    );
  // ---------------------- //

  // -------- AXIS -------- //
  // xAxis
  LG_xScale = d3.scaleLinear().range([0, graphDim.width]).domain([2000, 2020]);
  var LG_xAxis = d3.axisBottom(LG_xScale).ticks(4, "i");
  graph
    .append("g")
    .attr("id", "LG_xAxis")
    .style("color", "white")
    .attr("transform", "translate(0," + graphDim.heigth + ")")
    .call(LG_xAxis);

  // yAxis
  var yDomain;
  if (yValue == "votes") {
    yDomain = [0, 10];
  } else if (yValue == "income") {
    yDomain = [0, 110000000];
  }

  LG_yScale = d3.scaleLinear().range([graphDim.heigth, 0]).domain(yDomain);
  var LG_yAxis = d3.axisLeft(LG_yScale);
  graph
    .append("g")
    .attr("id", "LG_yAxis")
    .style("color", "white")
    .attr("transform", "translate(0," + ")")
    .call(LG_yAxis);

  // text label for the y axis
  graph
    .append("text")
    .attr("y", 0 - margin.top)
    .attr("x", 0 - margin.left / 2)
    .attr("dy", "1em")
    .style("fill", "white")
    .style("text-anchor", "middle")
    .text(function () {
      if (yValue == "votes") {
        return "Votes";
      } else if (yValue == "income") {
        return "Income ($)";
      }
    });

  // ---------------------- //
}

function changeLinegraph(genreList, dataReader) {
  d3.selectAll(".linegraphLine").remove(); // delete all lines
  genreList.forEach(function (genre) {
    dataReader.then(function (data) {
      var LG_data = data.filter(function (d) {
        return d.genre == genre;
      });
      LG_data = LG_data.filter(function (d) {
        return d.country == "USA";
      });

      //console.log(LG_data);
      drawLine(LG_data, genre);
    });
  });
  var delayInMilliseconds = 800; //800 millisecond

  setTimeout(function () {
    //your code to be executed after tot second
    updateLineGraph();
  }, delayInMilliseconds);
}

function drawLine(data, genre) {
  var avg_year = [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  ];
  var nfilm = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

  data.forEach(function (d) {
    var value = NaN;
    if (yValue == "votes") {
      value = d.avg_vote;
    } else if (yValue == "income") {
      value = d.usa_gross_income;
    }

    if (!isNaN(value)) {
      avg_year[d.year - 2000] += value;
    }
    nfilm[d.year - 2000]++;
  });

  var finalData = [];

  for (var i = 0; i < 21; i++) {
    avg_year[i] = avg_year[i] / nfilm[i];
    var obj = {
      year: 2000 + i,
      avg: avg_year[i].toFixed(1),
    };
    if (!isNaN(obj.avg)) {
      finalData.push(obj);
    }
  }

  var line = d3
    .line()
    .x(function (d) {
      return LG_xScale(d.year);
    })
    .y(function (d) {
      return LG_yScale(d.avg);
    })
    .curve(d3.curveLinear);
  var g = d3
    .select("#linegraph")
    .append("g")
    .attr("class", "linegraphLine")
    .attr("id", genre + "_line")
    .style("opacity", 0.8)
    .attr("fill", colorDict[genre])
    .attr("stroke", colorDict[genre]);
  g.append("path")
    .attr("fill", "none")
    .attr("stroke-width", "3px")
    .attr("d", line(finalData));

  g.selectAll(".LG_circle")
    .data(finalData)
    .enter()
    .append("circle")
    .attr("class", "LG_circle")
    .attr("id", function (d) {
      return genre + "-" + d.year;
    })
    .attr("cx", function (d) {
      return LG_xScale(d.year);
    })
    .attr("cy", function (d) {
      return LG_yScale(d.avg);
    })
    .attr("r", 0)
    .attr("stroke", "black")
    .on("click", function (c) {
      // Trigger Interaction //
      var interaction_data = data.filter(function (d) {
        return d.year == c.year;
      });
      triggerLineGraph(interaction_data);
      // ------------------- //
      d3.select(this).dispatch("mouseover");
    });
  var enter_duration = 500;

  g.selectAll(".LG_circle")
    .transition()
    .delay(function (d, i) {
      return (i / finalData.length) * enter_duration;
    })
    .attr("r", 5);

  g.selectAll(".LG_circle")
    .on("mouseover", function (d) {
      d3.select(this).attr("cursor", "pointer");
      d3.select(this).attr("r", (d3.select(this).attr("r") * 3) / 2);
      createTooltip(this, d, genre);
    })
    .on("mouseout", function (d, i) {
      d3.select(this).attr("cursor", "default");
      d3.select(this).attr("r", d3.select(this).attr("r") / (3 / 2)); // for transition: ...(this).transition().attr(...
      deleteTooltip();
    });
}

function createTooltip(object, data, genre) {
  var x =
    parseFloat(object.getAttribute("cx")) + (window.innerWidth * 4) / 12 + "px";
  var y =
    parseFloat(object.getAttribute("cy")) + (window.innerHeight * 1) / 6 + "px";

  var textY = "Errore";
  if (yValue == "votes") {
    textY = "Average Vote: " + data.avg;
  } else if (yValue == "income") {
    textY = "Average Income: " + numberWithCommas(data.avg) + "$";
  }

  d3.select(".linegraphView")
    .append("g")
    // class
    .attr("class", "linegraphTooltip")
    // position
    .style("position", "absolute")
    .style("left", x)
    .style("top", y)
    // appearance
    .style("opacity", 0)
    .style("background-color", "white")
    .style("border", "solid")
    .style("border-width", "2px")
    .style("border-radius", "5px")
    .style("padding", "5px")
    // text
    .html("Year: " + data.year + "<br>" + "Genre: " + genre + "<br>" + textY)
    // animation
    .transition()
    .delay(120)
    .duration(250)
    .style("opacity", 1);
}

function deleteTooltip() {
  d3.selectAll(".linegraphTooltip")
    .transition()
    .duration(120)
    .style("opacity", 0)
    .on("end", function () {
      this.remove();
    });
}
