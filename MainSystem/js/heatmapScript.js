var myData = [];
var mysvg, myColor, x, y, legend;
var selected = [];

function resetH() {
  for (var i = 0; i < 11; i++) {
    legend
      .selectAll("#rect" + i)
      .style("stroke", "none")
      .style("opacity", 0.8);
  }
  mysvg
    .selectAll(".rect")
    //.style("stroke", "none")
    .style("opacity", 0.8);
  selected = [];
  return;
}

function drawHeatmap(heatmapView, colValue) {
  d3.select("#mysvg").remove();

  var margin = { top: 64, right: 4, bottom: 64, left: 32 },
    width =
      parseInt(heatmapView.style("width"), 10) - margin.left - margin.right,
    height =
      parseInt(heatmapView.style("height"), 10) - margin.top - margin.bottom;

  mysvg = heatmapView
    .append("svg")
    .attr("id", "mysvg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  var myGroups = [
    2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011,
    2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020,
  ];
  var myVars = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  x = d3.scaleBand().range([0, width]).domain(myGroups).padding(0.05);
  mysvg
    .append("g")
    .style("font-size", 15)
    .style("color", "white")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x));

  y = d3.scaleBand().range([height, 0]).domain(myVars).padding(0.05);
  mysvg
    .append("g")
    .style("font-size", 15)
    .style("color", "white")
    .call(
      d3
        .axisLeft(y)
        .tickValues(myVars)
        .tickFormat(function (i) {
          var myMonths = [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec",
          ];
          return myMonths[i - 1];
        })
    );

  // Build color scale

  myColor = d3
    .scaleQuantize()
    .domain([0, 10])
    .range([
      "#EDF7CA",
      "#D9ED92",
      "#B5E48C",
      "#99D98C",
      "#76C893",
      "#52B69A",
      "#34A0A4",
      "#168AAD",
      "#1A759F",
      "#1E6091",
      "#184E77",
    ]);

  /*[
      "#67001f",
      "#b2182b",
      "#d6604d",
      "#f4a582",
      "#fddbc7",
      "#f7f7f7",
      "#d1e5f0",
      "#92c5de",
      "#4393c3",
      "#2166ac",
      "#053061",
    ]*/

  //Add a legend
  var legendText =
    colValue == "votes"
      ? ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
      : [
          "<10k",
          "10k",
          "20k",
          "30k",
          "40k",
          "50k",
          "60k",
          "70k",
          "80k",
          "90k",
          "100k",
        ];
  legend = d3
    .select("#mysvg")
    .append("g")
    .attr("class", "legend")
    .attr("width", 140)
    .attr("height", 20)
    .attr("transform", "translate(35, 5)")
    .selectAll("g")
    .data(myColor.ticks())
    .enter()
    .append("g")
    .attr("transform", function (i) {
      return colValue == "votes"
        ? "translate(" + i * 20 + ",0)"
        : "translate(" + i * 38 + ",0)";
    });

  selected = [];

  function toggle(id) {
    if (id == 11) {
      for (var i = 0; i < 11; i++) {
        legend
          .selectAll("#rect" + i)
          .style("stroke", "none")
          .style("opacity", 0.8);
      }
      mysvg.selectAll(".rect").style("stroke", "none").style("opacity", 0.8);
      selected = [];
      return;
    }
    if (selected.indexOf(id) < 0) {
      // if not already selected, select and show
      selected.push(id);
      for (var i = 0; i < 11; i++) {
        legend
          .select("#rect" + i)
          .style("stroke", function () {
            return selected.includes(i) ? "rosybrown" : "none";
          })
          .style("stroke-width", function () {
            return selected.includes(i) ? 3 : 0;
          })
          .style("opacity", function () {
            return selected.includes(i) ? 1 : 0.5;
          });
      }
      //legend.select("#rect" + id).style("stroke", "rosybrown").style("stroke-width",5);
      mysvg.selectAll(".rect").style("opacity", 0);
      for (var i = 0; i < selected.length; i++) {
        var el = selected[i];
        mysvg.selectAll("#blue" + el).style("opacity", 0.8);
      }
    } else {
      // if already selected, unselect and hide
      for (var i = 0; i < selected.length; i++) {
        if (selected[i] === id) {
          selected.splice(i, 1);
        }
      }
      legend
        .select("#rect" + id)
        .style("stroke", "none")
        .style("opacity", 0.5);
      mysvg.selectAll("#blue" + id).style("opacity", 0);
    }
  }

  legend
    .append("rect")
    .attr("width", 18)
    .attr("height", 15)
    .style("fill", myColor)
    .data(myColor.ticks())
    .attr("id", function (i) {
      return "rect" + i;
    })
    .style("opacity", 0.8)
    .on("mouseover", function () {
      this.style.cursor = "pointer";
    })
    .on("click", function (i) {
      return toggle(i);
    });

  legend
    .append("text")
    .data(legendText)
    .attr("x", 1.5)
    .attr("y", 25)
    .attr("dy", ".35em")
    .text(function (d) {
      return d;
    })
    .style("fill", "white");
}

async function fillHeatmap(
  heatmapView,
  dataReader,
  selected_genreList,
  colVal
) {
  //Read the data
  dataReader.then(function (data) {
    // Data-filtering on country

    myData = data.filter(function (d) {
      return d.country.indexOf("USA") >= 0;
    });

    // Data-filtering on genre

    myData = myData.filter(function (d) {
      if (selected_genreList != "All") {
        return selected_genreList.indexOf(d.genre) >= 0;
      } else {
        return d.genre != "";
      }
    });

    // Grouping data for extracting mean value
    var groupedData = d3
      .nest()
      .key(function (d) {
        return d.year;
      })
      .sortKeys(d3.ascending)
      .key(function (d) {
        return d.month_published;
      })
      .sortKeys(d3.descending)
      .rollup(function (d) {
        return {
          vote: d3.mean(d, function (v) {
            return colVal == "votes"
              ? +v.avg_vote
              : +(v.usa_gross_income / 10000000);
          }),
        };
      })
      .entries(myData);

    var dict = new Object();
    for (i = 0; i < groupedData.length; i++) {
      //console.log(groupedData[i].key)			//year
      year = groupedData[i].key;
      for (j in groupedData[i]) {
        for (z = 0; z < groupedData[i][j].length; z++) {
          //console.log(groupedData[i][j][z].key)   		//month
          month = groupedData[i][j][z].key;
          for (w in groupedData[i][j][z]) {
            //console.log( groupedData[i][j][z][w].vote) 	//vote
            vote = groupedData[i][j][z][w].vote;
            dict[year + "," + month] = Math.round(vote);
          }
        }
      }
    }

    // create a tooltip
    function createhTooltip() {
      var tooltip = heatmapView
        .append("div")
        .style("position", "absolute")
        .style("opacity", 0)
        .attr("class", "tooltip")
        .attr("id", "hTooltip")
        .style("background-color", "white")
        .style("border", "solid")
        .style("border-width", "2px")
        .style("border-radius", "5px")
        .style("padding", "5px");

      return tooltip;
    }

    // Three function that change the tooltip when user hover / move / leave a cell
    var mouseover = function (d) {
      this.style.cursor = "pointer";
      if (
        d3.select(this).style("opacity") == 0.02 ||
        d3.select(this).style("opacity") == 0.8 ||
        d3.select(this).style("opacity") == 1
      ) {
        tooltip = createhTooltip();

        tooltip
          .style("opacity", 1)
          .html(function () {
            return colVal == "votes"
              ? "The Average <br> Vote is: " +
                  dict[d.year + "," + d.month_published]
              : "The Average Income of<br>this month and year is: " +
                  dict[d.year + "," + d.month_published] +
                  "0k";
          })
          .style("left", d3.mouse(this)[0] + "px")
          .style("top", d3.mouse(this)[1] + "px");

        d3.select(this)
          .style("stroke", "white") //#BF1A2F
          .style("stroke-width", "5px")

      }
    };

    var mouseleave = function () {
      this.style.cursor = "default";
      //if (d3.select(this).style("opacity") == 1) {
      if(d3.select(this).style("opacity") == 0.02){
        d3.select("#hTooltip").remove();
      }
      if (
        d3.select(this).style("opacity") == 0.8 ||
        d3.select(this).style("opacity") == 1
      ) {
        d3.select("#hTooltip").remove();

        d3.select(this)
          .style("stroke", function () {
            if (interactionData.includes(this)) {
              return "white";
            } else {
              return "none";
            }
          })
          .style("stroke-width", function () {
            interactionData.includes(this) ? "5px" : "0px";
          })
          .style("opacity", function () {
            interactionData.includes(this) ? 1 : 0.8;
          });
      }
    };

    // add the squares
    var Cells = mysvg
      .selectAll()
      .data(myData, function (d) {
        return d.year + ":" + d.month_published;
      })
      .enter()
      .append("rect")
      .attr("class", "rect")
      .attr("x", function (d) {
        return x(d.year);
      })
      .attr("y", function (d) {
        return y(d.month_published);
      })
      .attr("rx", 4)
      .attr("ry", 4)
      .attr("width", x.bandwidth())
      .attr("height", y.bandwidth())
      .attr("id", function (d) {
        if (dict[d.year + "," + d.month_published] > 10) {
          return "blue10";
        }
        return "blue" + dict[d.year + "," + d.month_published];
      })
      .style("fill", function (d) {
        if (dict[d.year + "," + d.month_published] > 10) {
          return myColor(10);
        }
        return myColor(dict[d.year + "," + d.month_published]);
      })
      .style("opacity", 0.8)
      .style("stroke", function (d) {
        return interactionData.includes(d) ? "white" : "none";
      })
      .style("stroke-width", function (d) {
        return interactionData.includes(d) ? "5px" : "0px";
      })
      .on("mouseover", mouseover)
      //.on("mousemove", mousemove)
      .on("mouseout", mouseleave)
      .on("click", function (d) {
        if (d3.select(this).style("opacity") == 0) {
          return;
        } else {
          return triggerHeatmap(d);
        }
      });

    for (var i = 0; i < 11; i++) {
      legend.select("#rect" + i).style("stroke", "none");
      legend
        .select("#rect" + i)
        .style("fill", myColor)
        .on("mouseover", function () {
          this.style.cursor = "pointer";
        });
      if (mysvg.selectAll("#blue" + i).empty()) {
        legend
          .select("#rect" + i)
          .style("fill", "#eae9e9")
          .on("mouseover", function () {
            this.style.cursor = "default";
          });
      }
    }
  });
}

function dataSelection(heatmapView, dataReader, genreList, colVal) {
  mysvg.selectAll(".rect").remove();
  //console.log("svg",svg)
  fillHeatmap(heatmapView, dataReader, genreList, colVal);
  for (var i = 0; i < 11; i++) {
    legend
      .selectAll("#rect" + i)
      .style("stroke", "none")
      .style("opacity", 0.8);
  }
  selected = [];
}
