//we want to select a month, a list of actors and one or plus genres then we want to prijnt the 5 most succesful directors

function directors(genre, month) {
  // reset predictions
  d3.select(".results").html("");

  let error = "Values missing: <br>";
  let flag = false;
  if (genre == "init") {
    error = error.concat("genre<br>");
    flag = true;
  }

  if (castList.length === 0) {
    error = error.concat("Actors<br>");
    flag = true;
  }
  if (flag) {
    d3.select("#registi").html(error);
  } else {
    dataReader.then(function (data) {
      let direct = [];
      let movies = data.filter(
        (element) =>
          +element.month_published === +month && element.genre === genre
      );
      var media_voto = 0;
      var media_income = 0;
      var prediction = 0;
      for (elm of movies) {
        media_voto += elm.avg_vote;
        media_income += elm.usa_gross_income;
      }
      media_income = media_income / movies.length
      media_voto = media_voto / movies.length
      var new_movies = movies.filter((d) =>
        d.actors.split(",").some((item) => castList.includes(item))
      );
      if (new_movies.length > 0) {
        
        var media_attori = 0;
        parallelReader.then(function (attori) {
          attori = attori.filter((elm) => castList.includes(elm.name))
          for (a of attori) {
            media_attori += a[genre]
          }
          media_attori = media_attori / castList.length

          new_movies = new_movies.sort(function (a, b) {
            return a.usa_gross_income - b.usa_gross_income;
          });

          direct = new_movies.map((d) => d.director.split(", ")[0]);

          if (direct.length > 5) {
            direct = direct.slice(0, 5);
          }
          prediction = (media_attori + media_voto) / 2
          prediction = prediction.toFixed(2)
          media_income = (0.8 * prediction + media_income) / 2
          media_income = media_income.toFixed(2)

          direct.unshift('Select a director')
          d3.select("#registi").html("");
          var radio = d3.select("#registi")
            .append('select')
            .attr("class", "dire")
            .on("change", function () { fun_prediction(prediction, media_income,genre); })
          ;

          radio // Add a button
            .selectAll('myOptions') // Next 4 lines add 6 options = 6 colors
            .data(direct)
            .enter()
            .append('option')
            .text(function (d) { return d; }) // text showed in the menu
            .attr("value", function (d) { return d; }) // corresponding value returned by the button
          ;
        })
      }
      else {
        direct = movies.map((d) => d.director.split(", ")[0]);
        if (direct.length > 5) {
          direct = direct.slice(0, 5);
        }
        var prediction = media_voto.toFixed(2)
        media_income = (0.8 * prediction + media_income) / 2
        media_income = media_income.toFixed(2)

        direct.unshift('Select a director')
        d3.select("#registi").html("");
        var radio = d3.select("#registi")
          .append('select')
          .attr("class", "dire")
          .on("change", function () { fun_prediction(prediction, media_income,genre); })
        ;
        radio // Add a button
          .selectAll('myOptions') // Next 4 lines add 6 options = 6 colors
          .data(direct)
          .enter()
          .append('option')
          .text(function (d) { return d; }) // text showed in the menu
          .attr("value", function (d) { return d; }) // corresponding value returned by the button
        ;
      }
        
      
    });
  }
}

function fun_prediction(prediction, media_income,genre) {
  prediction = +prediction
  media_income = +media_income
  var regista = d3.select(".dire").property("value") //select value
  dataReader.then(function (data) {
    let media_regista = 0;
    let guadagno = 0;
    let i = 0;
    for (d of data) {
      if (d.director.split(", ")[0] === regista && d.genre===genre) {
        i += 1
        media_regista += +d.avg_vote
        guadagno += +d.usa_gross_income
      }
    }
    media_regista = media_regista / i
    prediction = (prediction + media_regista) / 2
    media_income = (media_income + guadagno) / 2
    let printable = "Vote (out of 10): " + prediction.toFixed(2) + "<br>Income ($): " + numberWithCommas(media_income.toFixed(2))
    d3.select(".results").html(printable);
  })

}
