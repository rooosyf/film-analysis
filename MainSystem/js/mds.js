var mdssvg;

//compute distance between dots
function getDistance(xA, yA, xB, yB) {
  var xDiff = Math.abs(xA - xB);
  var yDiff = Math.abs(yA - yB);

  return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
}

function drawMDSgraph(mdsView) {
  // set the dimensions and margins of the graph
  var margin = { top: 12, right: 32, bottom: 32, left: 67 },
    width = parseInt(mdsView.style("width"), 10) - margin.left - margin.right,
    height = parseInt(mdsView.style("height"), 10) - margin.top - margin.bottom;

  // append the svg object to the body of the page
  mdssvg = mdsView
    .append("svg")
    .attr("id", "mdssvg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
}

function searchFilm() {

  // get the search value
  var search = document.getElementById("filmsearch").value;
  
  similarFilms(search);
}
  

function similarFilms(search){
  console.log("3")
  
  // change the value in the search bar
  document.getElementById("filmsearch").value = search

  
  search = search.toLowerCase();
  
  // get the film title NOT to lower case, so as to print it
  var film_title = mdssvg.selectAll("circle")
    .filter(function (d) {
     
      return d.Label.toLowerCase() == search;
    })._groups[0][0].__data__["Label"]
  ;

  printFilms([film_title]);

  


  // get the actors of the searched film and print them
  dataReader.then(function (data) {

    var film_actors = [];

    data.forEach(function (d) {

      if (d.original_title == film_title) {

        film_actors = d.actors.split(",");
        printActors(film_actors)
      }

    });

  });

  // unhighlight the unselected films

  mdssvg.selectAll("circle")
    .style("fill", "gray")
    .style("opacity", 0.05)
    .style("stroke", "black")


  // get the similar films based on the current MDS and print them

  var xA = mdssvg
    .selectAll("circle")
    .filter(function (d) {
      return d.Label.toLowerCase() == search;
    })
    .property("x");

  var yA = mdssvg
    .selectAll("circle")
    .filter(function (d) {
      return d.Label.toLowerCase() == search;
    })
    .property("y");

  var similar_titles = [];

  var nearDots = mdssvg.selectAll("circle").each(function (d) {

    var xB = d.X;
    var yB = d.Y;
    d.distance = getDistance(xA, yA, xB, yB);

    if (d.distance < 19 && d.Label.toLowerCase() != search) {

      similar_titles.push(d.Label);

    }

  });
  similar_titles.push("Similar Films")

  printFilms(similar_titles)

  nearDots.style("fill", "gray")
  
  // highlight the similar films

  nearDots.filter(d => similar_titles.includes(d.Label))
        .style("fill", d => colorDict[d.genre])
        .style("opacity", 0.35)

  
  // highlight the selected film

  mdssvg.selectAll("circle")
        .filter(d => d.Label.toLowerCase() == search)
        .style("fill", function(d){
          return colorDict[d.genre]}  )
        .style("opacity", 1)
        .style("stroke", "white")

  
  parallellFilter([film_title]);

}

function fillMDSgraph(mdsView, file) {

  // remove all dots and axes
  mdssvg.selectAll("circle").remove();
  mdssvg.select("#MDSaxis--x").remove();
  mdssvg.select("#MDSaxis--y").remove();


  // set width and height
  var width = parseInt(mdsView.style("width"), 10) - 80;
  height = parseInt(mdsView.style("height"), 10) - 80;

  //load data
  d3.csv("data/MDS" + file + ".csv").then(function (data) {

    // Add X axis
    var x = d3
      .scaleLinear()
      .domain(
        d3.extent(data, function (d) {
          return +d.X;
        })
      )
      .range([0, width]);

    mdssvg
      .append("g")
      .attr("id", "MDSaxis--x")
      .style("color", "white")
      .attr("transform", "translate(0," + height + ")")
      .transition()
      .duration(1000)
      .call(d3.axisBottom(x));

    // Add Y axis
    var y = d3
      .scaleLinear()
      .domain(
        d3.extent(data, function (d) {
          return +d.Y;
        })
      )
      .range([height, 0]);

    mdssvg
      .append("g")
      .attr("id", "MDSaxis--y")
      .style("color", "white")
      .transition()
      .duration(1000)
      .call(d3.axisLeft(y));

    // OLD BRUSH
    /*var brush = d3
      .brush()
      .extent([
        [0, 0],
        [width, height],
      ])
      .on("end", brushended),
      idleTimeout,
      idleDelay = 350;*/

    // add brush
    var brush = d3
      .brush()
      .extent([
        [0, 0],
        [width, height],
      ])
      .on("brush", onBrush)
      .on("end", onEnd);

    var clip = mdssvg
      .append("defs")
      .append("svg:clipPath")
      .attr("id", "clip")
      .append("svg:rect")
      .attr("width", width)
      .attr("height", height)
      .attr("x", 0)
      .attr("y", 0);

    //add scatter area
    var scatter = mdssvg
      .append("g")
      .attr("id", "scatterplot")
      .attr("clip-path", "url(#clip)");

    //add tooltip and its functions
    function createmTooltip() {

      var tooltip = mdsView
        .append("div")
        .attr("id", "mTooltip")
        .attr("class", "tooltip")
        .style("position", "absolute")
        .style("opacity", 0)
        .style("background-color", "white")
        .style("border", "solid")
        .style("border-width", "1px")
        .style("border-radius", "5px")
        .style("padding", "10px");

      return tooltip;
    }

    var mouseover = function (d) {

      tooltip = createmTooltip();

      tooltip
        .style("opacity", 1)
        .html(d.Label)
        .style("right", d3.mouse(this)[0] + "px")
        .style("top", d3.mouse(this)[1] + "px");
    };



    // A function that change this tooltip when the leaves a point: just need to set opacity to 0 again
    var mouseleave = function (d) {

      d3.select("#mTooltip").remove().transition().duration(1);

    };

    


    var flag = false;
      // Add dots
     scatter
      .append("g")
      .selectAll("dot")
      .data(data)
      .enter()
      .append("circle")
      .property("x", function (d) {
        return d.X;
      })
      .property("y", function (d) {
        return d.Y;
      })
      .attr("cx", function (d) {
        return x(d.X);
      })
      .attr("cy", function (d) {
        return y(d.Y);
      })
      .transition()
      .duration(1000)
      .attr("r", 3)
      .style("fill", function (d) {

        return colorDict[d.genre];
      })
      .style("opacity", 0.3)
      .style("stroke", "black");
    
    flag = true;

    // add the brush OVER the dots
    scatter.append("g").attr("class", "brushMDS").call(brush);

    // set on over, leave and click
    scatter
      .selectAll("circle")
      .on("mouseover", mouseover)
      //.on("mousemove", mousemove)
      .on("mouseleave", mouseleave)
      .on("click", function (d) {
        triggerMDS(d);
      })
      .on("mousedown", function () {
        brush_elm = scatter.select(".brushMDS").node();
        new_click_event = new Event("mousedown");
        new_click_event.pageX = d3.event.pageX;
        new_click_event.clientX = d3.event.clientX;
        new_click_event.pageY = d3.event.pageY;
        new_click_event.clientY = d3.event.clientY;
        brush_elm.dispatchEvent(new_click_event);
      });

    // on brush function
    function onBrush() {
      // brushSelection gives us the coordinates of the
      // top left and bottom right of the brush box

      // if brush.null -> reset mds
      if (d3.brushSelection(this) === null) {

        titles = [];

        scatter
          .selectAll("circle")
          .style("fill", function (d) {
            return colorDict[d.genre];
          })
          .style("stroke", "black")
          .style("opacity", 0.3);

        printFilms(titles)

      }

      else {
        const [[x1, y1], [x2, y2]] = d3.brushSelection(this);

        // return true if the dot is in the brush box, false otherwise
        function isBrushed(d) {
          const cx = x(d.X);
          const cy = y(d.Y);
          return cx >= x1 && cx <= x2 && cy >= y1 && cy <= y2;
        }

        // style the dots that are selected

        // OLD condition used when there was intersection
        if (seltitles.length > 0) {

          scatter
            .selectAll("circle")
            .style("fill", function (d) {
              return isBrushed(d) && seltitles.includes(d.Label)
                ? colorDict[d.genre]
                : "gray";
            })
            .style("stroke", function (d) {
              return isBrushed(d) && seltitles.includes(d.Label)
                ? "black"
                : "black";
            })
            .style("opacity", function (d) {
              return isBrushed(d) && seltitles.includes(d.Label) ? 1 : 0.3;
            });
        }

        // actual styling of brushed dots
        else {

          handler_of_brushes();
          scatter.selectAll("circle")
            .style("fill", function (d) {
              return isBrushed(d) ? colorDict[d.genre] : "gray";
            });
        }
      }
    }

    // brush ended function
    function onEnd() {

      // if the brush is cleared -> reset MDS
      if (d3.brushSelection(this) === null) {

        titles = [];

        scatter
          .selectAll("circle")
          .style("fill", function (d) {
            return colorDict[d.genre];
          })
          .style("stroke", "black")
          .style("opacity", 0.3);


        printFilms(titles)

      }
      else {

        const [[x1, y1], [x2, y2]] = d3.brushSelection(this);

        // return true if the dot is in the brush box, false otherwise
        function isBrushed(d) {
          const cx = x(d.X);
          const cy = y(d.Y);
          return cx >= x1 && cx <= x2 && cy >= y1 && cy <= y2;
        }

        // OLD selection, when there was intersection, now seltitles is always empty
        var brushed =
          seltitles.length > 0
            ? scatter.selectAll("circle").filter(function (d) {
              return isBrushed(d) && seltitles.includes(d.Label);
            })
            : scatter.selectAll("circle").filter(function (d) {
              return isBrushed(d);
            });

        // get the brushed dots
        brushed = brushed._groups[0];

        // get the labels of the brushed dots and print them
        titles = [];
        brushed.forEach((d) => {
          const title = d.__data__.Label;
          titles.push(title);
        });

        printFilms(titles)



      }

      // trigger parallel -> select the actors that starred in the brushed movies
      trigger_parallel_from_MDS(titles);
    }

    // add brush functions
    mdsView.select(".brushbutton").on("click", function () {

      scatter.select(".brushMDS").call(brush.move, null);
    });


    
    
  });
  // needed when we change the type of MDS -> updates the list of similar films according to it
    
  setTimeout(function(){
    if (document.getElementById("filmsearch").value != "") {
    console.log(mdssvg.selectAll("circles")._groups[0].length)
    searchFilm();
    
    
    
  }}, 2000)
}

// reset-dots function 
function resetMDS() {

  mdssvg
    .selectAll("circle")
    .style("fill", function (d) {
      return colorDict[d.genre];
    })
    .style("opacity", 0.3)
    .style("stroke", "black");
}

// print films function
function printFilms(titles) {

  // if no titles to print -> remove everything and print "default"
  if (titles.length === 0) {

    d3.selectAll("#filmss").remove()
    d3.selectAll("#filmsS").remove()
    d3.selectAll("#def").remove()

    d3.select(".film_title")
      .append("div")
      .attr("id", "def")
      .text("Use the brush on the MDS  graph to print films titles")
      .style("text-align", "center");
    return;
  }
  else {

    var sim = titles[titles.length - 1] == "Similar Films"

    // if printing similar films -> don't remove the film printed (already removed by the print of the searched film) and print 'Similar Film' + films
    if (sim) {

      d3.selectAll("#def").remove()

      d3.select(".film_title")
        .append("div")
        .attr("id", "filmsS")
        .style("font-weight", "bold")
        .style("margin-top", "1em")
        .style("margin-bottom", "1em")
        .style("font-size", "1.17em")
        .text("Similar Films")

      titles.pop()
      titles.forEach(e =>
        d3.select(".film_title")
          .append("div")
          .attr("id", "filmss")
          .style("border", "white .1vh solid")
          .style("stroke", "white")
          .style("border-radius", "15px")
          .style("padding", "10px")
          .style("margin-top", "3px")
          .text(e)
      )
    }
    else {

      d3.selectAll("#def").remove()
      d3.selectAll("#filmsS").remove()
      d3.selectAll("#filmss").remove()
      titles.forEach(e =>
        d3.select(".film_title")
          .append("div")
          .attr("id", "filmss")
          .style("border", "white .1vh solid")
          .style("stroke", "white")
          .style("border-radius", "15px")
          .style("padding", "10px")
          .style("margin-top", "3px")
          .text(e)
      )
    
    }

    
    


    dataReader.then(function (data) {
      var dots = d3.selectAll(".genre_dot")._groups[0].length
      if (dots == 0) {
        d3.selectAll("#filmss")
          .append("div")
          .attr("class", "genre_dot")
          .style("background", function () {
            // get the text of that card
            var title = this.previousSibling.data

            // get the tuple corresponding to that title
            var e = data.filter(d => d.original_title == title)

            // fill the dot with the color associated with the genre of that film
            return colorDict[e[0].genre]
          })
          .style("float", "right")


        d3.selectAll("#filmss")
          .append("div")
          .style("font-size", "0.8em")
          .text(function () {

            // get the text of that card
            var title = this.parentNode.childNodes[0].data

            // get the tuple corresponding to that title
            var e = data.filter(d => d.original_title == title)

            return "Vote: " + e[0].avg_vote;
          }

          )
      }



    })

    d3.selectAll("#filmss")
      .on("click", function(){ 
       
        similarFilms(this.firstChild.data);
        
      })
      .on("mouseover", function(){
        // cursor to pointer
        d3.select(this).style("cursor", "pointer");
      })
      .on("mouseout", function () {
        // cursor to default
        d3.select(this).style("cursor", "default");
      })
    ;
  }


}